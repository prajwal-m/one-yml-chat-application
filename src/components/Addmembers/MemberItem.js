import React from "react";

const MemberItem = ({ text, itemIndex, removeItem }) => {
  const deleteItem = itemIndex => {
    console.log("deleted", itemIndex);
    removeItem(itemIndex);
  };
  return (
    <div className="member-card-container">
      <label className="form-check-label">{text}</label>
      <button
        type="button"
        className="form-check-button"
        onClick={() => deleteItem(itemIndex)}
      >
        x
      </button>
    </div>
  );
};
export default MemberItem;
